Spatial interpolation / prediction using Ensemble Machine Learning
================
Created and maintained by: Tom Hengl
(<a href="mailto:tom.hengl@OpenGeoHub.org" class="email">tom.hengl@OpenGeoHub.org</a>)
\|
Last compiled on: 30 October, 2020



-   [![alt text](tex/R_logo.svg.png "About")
    Introduction](#alt-text-introduction)
    -   [Ensemble Machine Learning](#ensemble-machine-learning)
    -   [Using geographical distances to improve spatial
        interpolation](#using-geographical-distances-to-improve-spatial-interpolation)
    -   [EML using the landmap package](#eml-using-the-landmap-package)
-   [![alt text](tex/R_logo.svg.png "Zinc") Example: Spatial prediction
    of Zinc meuse data
    set](#alt-text-example-spatial-prediction-of-zinc-meuse-data-set)
    -   [Interpolation of numeric values using spatial
        regression](#interpolation-of-numeric-values-using-spatial-regression)
    -   [Estimation of prediction
        intervals](#estimation-of-prediction-intervals)
    -   [Predictions using log-transformed target
        variable](#predictions-using-log-transformed-target-variable)
-   [![alt text](tex/R_logo.svg.png "Zinc") Example: Spatial prediction
    of soil types
    (factor-variable)](#alt-text-example-spatial-prediction-of-soil-types-factor-variable)
    -   [EML for classification](#eml-for-classification)
    -   [Classification accuracy](#classification-accuracy)
-   [![alt text](tex/R_logo.svg.png "Multi-scale") Example: Spatial
    prediction of soil organic carbon using 2-scale
    model](#alt-text-example-spatial-prediction-of-soil-organic-carbon-using-2-scale-model)
    -   [Coarse-scale model](#coarse-scale-model)
    -   [Fine-scale model](#fine-scale-model)
    -   [Merge multi-scale predictions](#merge-multi-scale-predictions)
-   [Summary notes](#summary-notes)
-   [References](#references)

[<img src="tex/opengeohub_logo_ml.png" alt="OpenGeoHub logo" width="350"/>](https://opengeohub.org)

![alt text](tex/R_logo.svg.png "About") Introduction
----------------------------------------------------

#### Ensemble Machine Learning

Ensembles are predictive models that combine predictions from two or
more learners (Seni & Elder, [2010](#ref-seni2010ensemble); Zhang & Ma,
[2012](#ref-zhang2012ensemble)). The specific benefits on using Ensemble
learners are:

-   **Performance**: they can help improve the average prediction
    performance over any individual contributing learner in the
    ensemble.
-   **Robustness**: they can help reduce extrapolation / overshooting
    effects of individual learners.
-   **Unbiasness**: they can help determine a model-free estimate of
    prediction errors.

Even the most flexible and best performing learners such as Random
Forest or neural networks always carry a bias in the sense that the
fitting produces recognizable patterns and these are limited by the
properties of the algorithm. In the case of ensembles, the modeling
algorithm becomes secondary, and even though the improvements in
accuracy are often minor, there is a good chance that the final model
will be less prone to overshooting and extrapolation problems.

There are in principle three ways to apply ensembles (Zhang & Ma,
[2012](#ref-zhang2012ensemble)):

-   *bagging*: learn in parallel, then combine using some deterministic
    principle (e.g. weighted averaging),
-   *boosting*: learn sequentially in an adaptive way, then combine
    using some deterministic principle,
-   *stacking*: learn in parallel, then fit a meta-model to predict
    ensemble estimates,

The “meta-model” is an additional model that basically combines all
individual or “base learners”.

There are several packages in R for Ensemble Machine Learning, for
example:

-   [SuperLearner](https://cran.r-project.org/web/packages/SuperLearner/vignettes/Guide-to-SuperLearner.html)
    package,
-   [caretEnsemble](https://cran.r-project.org/web/packages/caretEnsemble/vignettes/caretEnsemble-intro.html)
    package,
-   [h2o.stackedEnsemble](http://docs.h2o.ai/h2o-tutorials/latest-stable/tutorials/ensembles-stacking/index.html)
    package,
-   [mlr](https://mlr.mlr-org.com/reference/makeStackedLearner.html) and
    [mlr3](https://mlr3gallery.mlr-org.com/posts/2020-04-27-tuning-stacking/)
    packages,

In this tutorial we focus primarily on using mlr package, i.e. wrapper
functions to mlr implemented in the landmap package.

#### Using geographical distances to improve spatial interpolation

Machine Learning was for long time been suboptimal for spatial
interpolation problems in comparison to classifical geostatistical
techniques such as kriging. To incorporate spatial dependence structures
in machine learning one can add so-called “geographical features”,
buffer distance, oblique distances, and/or distances in the watershed,
as features. This has shown to produce similar results as kriging, so
that there is in principle no further need for kriging of residuals or
similar (Hengl & MacMillan, [2019](#ref-hengl2019predictive); Hengl,
Nussbaum, Wright, Heuvelink, & Gräler, [2018](#ref-hengl2018random)).

Use of geographical as features in machine learning for spatial
predictions is explained in detail in:

-   Hengl, T., Nussbaum, M., Wright, M. N., Heuvelink, G. B., &
    Gräler, B. (2018). [Random forest as a generic framework for
    predictive modeling of spatial and spatio-temporal
    variables](https://doi.org/10.7717/peerj.5518). PeerJ, 6, e5518.
    <a href="https://doi.org/10.7717/peerj.5518" class="uri">https://doi.org/10.7717/peerj.5518</a>
-   Møller, A. B., Beucher, A. M., Pouladi, N., and Greve, M. H. (2020).
    [Oblique geographic coordinates as covariates for digital soil
    mapping](https://doi.org/10.5194/soil-6-269-2020). SOIL, 6, 269–289,
    <a href="https://doi.org/10.5194/soil-6-269-2020" class="uri">https://doi.org/10.5194/soil-6-269-2020</a>
-   Behrens, T., Schmidt, K., Viscarra Rossel, R. A., Gries, P.,
    Scholten, T., & MacMillan, R. A. (2018). [Spatial modelling with
    Euclidean distance fields and machine
    learning](https://doi.org/10.1111/ejss.12687). European journal of
    soil science, 69(5), 757-770.
-   Sekulić, A., Kilibarda, M., Heuvelink, G.B., Nikolić, M., Bajat, B.
    (2020). [Random Forest Spatial
    Interpolation](https://doi.org/10.3390/rs12101687). Remote Sens.
    12, 1687.
    <a href="https://doi.org/10.3390/rs12101687" class="uri">https://doi.org/10.3390/rs12101687</a>

In the case the number of covariates / features becomes large and
diverse, and in the case the points are equally spread in an area of
interest, there is probably no need of using geographical distances
because unique combinations of features are so many that they can be
used to represent *geographical position* (Hengl et al.,
[2018](#ref-hengl2018random)).

#### EML using the landmap package

Ensemble Machine Learning (EML) based on fitting a super learner is
implemented in the mlr package (Bischl et al.,
[2016](#ref-bischl2016mlr)) and can be initiated via the function:

    m = makeStackedLearner(base.learners = lrns, super.learner = "regr.ml", method = "stack.cv")

The base learner predictions will be computed by 5-fold cross-validation
(repeated re-fitting) and then used to determine the meta-learner. This
algorithm is known as the *SuperLearner* algorithm (Polley & van der
Laan, [2010](#ref-Polley2010)). In the case of spatial prediction
problem we want to block the learners based on spatial proximity to
prevent from producing bias predictions. For this we should know the
range of spatial dependence or similar i.e. something that can be
derived by fitting a variogram.

To automate fitting of an Ensemble Machine Learning models for the
purpose of spatial interpolation / prediction, one can now use the
[landmap](https://github.com/Envirometrix/landmap) package that
combines:

-   derivation of geographical distances,
-   conversion of grids to principal components,
-   automated filling of gaps,
-   automated fitting of variogram and determination of spatial
    auto-correlation structure,
-   spatial overlay,
-   model training using spatial Cross-Validation (Lovelace, Nowosad, &
    Muenchow, [2019](#ref-lovelace2019geocomputation)),
-   model stacking i.e. fitting of the final EML,

To install the most recent landmap package from Github use:

    library(devtools)
    install_github("envirometrix/landmap")

![alt text](tex/R_logo.svg.png "Zinc") Example: Spatial prediction of Zinc meuse data set
-----------------------------------------------------------------------------------------

#### Interpolation of numeric values using spatial regression

We can use the landmap package to fit a 2D model to interpolate zinc
concentration based on sampling points, distance to the river and
flooding frequency maps:

    library(landmap)
    library(rgdal)
    library(geoR)
    library(plotKML)
    library(raster)
    library(glmnet)
    library(xgboost)
    library(kernlab)
    library(deepnet)
    library(mlr)
    demo(meuse, echo=FALSE)
    m <- train.spLearner(meuse["zinc"], covariates=meuse.grid[,c("dist","ffreq")], 
                         lambda = 1, parallel=FALSE)

    ## Converting ffreq to indicators...

    ## Converting covariates to principal components...

    ## Deriving oblique coordinates...TRUE

    ## Fitting a variogram using 'linkfit' and trend model...TRUE

    ## Estimating block size ID for spatial Cross Validation...TRUE

    ## Using learners: regr.ranger, regr.xgboost, regr.nnet, regr.ksvm, regr.cvglmnet...TRUE

    ## Fitting a spatial learner using 'mlr::makeRegrTask'...TRUE

    ## [20:31:14] WARNING: amalgamation/../src/objective/regression_obj.cu:170: reg:linear is now deprecated in favor of reg:squarederror.
    ## [20:31:14] WARNING: amalgamation/../src/objective/regression_obj.cu:170: reg:linear is now deprecated in favor of reg:squarederror.
    ## [20:31:15] WARNING: amalgamation/../src/objective/regression_obj.cu:170: reg:linear is now deprecated in favor of reg:squarederror.
    ## [20:31:15] WARNING: amalgamation/../src/objective/regression_obj.cu:170: reg:linear is now deprecated in favor of reg:squarederror.
    ## [20:31:15] WARNING: amalgamation/../src/objective/regression_obj.cu:170: reg:linear is now deprecated in favor of reg:squarederror.
    ## [20:31:15] WARNING: amalgamation/../src/objective/regression_obj.cu:170: reg:linear is now deprecated in favor of reg:squarederror.
    ## # weights:  103
    ## initial  value 47710812.879921 
    ## final  value 17368518.991935 
    ## converged
    ## # weights:  103
    ## initial  value 42861532.304933 
    ## final  value 15655634.669355 
    ## converged
    ## # weights:  103
    ## initial  value 43165612.933940 
    ## final  value 16849328.088710 
    ## converged
    ## # weights:  103
    ## initial  value 38549610.273874 
    ## final  value 14174062.967742 
    ## converged
    ## # weights:  103
    ## initial  value 47423664.876468 
    ## final  value 18757150.733871 
    ## converged
    ## # weights:  103
    ## initial  value 54935644.485561 
    ## final  value 20750447.509677 
    ## converged

    ## Fitting a quantreg model using 'ranger::ranger'...TRUE

This runs number of steps including derivation of geographical distances
(Møller, Beucher, Pouladi, & Greve, [2020](#ref-moller2020oblique)),
derivation of principal components, fitting of variogram using the geoR
package (Diggle, Tawn, & Moyeed, [1998](#ref-diggle1998model)), spatial
overlay, training of individual learners and training of the super
learner. The only parameter we need to set manually here is the
`lambda = 1` which is required to estimate variogram. In this case the
target variable is log-normally distributed and hence the geoR package
needs the transformation parameter set at `lambda = 1`.

Note that in this case, the meta-learner is by default a linear model
from five independently fitted learners
`c("regr.ranger", "regr.xgboost", "regr.ksvm", "regr.nnet", "regr.cvglmnet")`.
We can check the success of training based on the 5-fold spatial
Cross-Validation using:

    summary(m@spModel$learner.model$super.model$learner.model)

    ## 
    ## Call:
    ## stats::lm(formula = f, data = d)
    ## 
    ## Residuals:
    ##     Min      1Q  Median      3Q     Max 
    ## -503.84  -98.56  -32.27   38.89 1311.41 
    ## 
    ## Coefficients:
    ##                Estimate Std. Error t value Pr(>|t|)    
    ## (Intercept)   673.26217  477.99206   1.409    0.161    
    ## regr.ranger     0.84013    0.19554   4.296 3.12e-05 ***
    ## regr.xgboost   -0.16878    0.44675  -0.378    0.706    
    ## regr.nnet      -1.45176    1.02600  -1.415    0.159    
    ## regr.ksvm       0.36441    0.22304   1.634    0.104    
    ## regr.cvglmnet  -0.08296    0.19157  -0.433    0.666    
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Residual standard error: 216.6 on 149 degrees of freedom
    ## Multiple R-squared:  0.6632, Adjusted R-squared:  0.6519 
    ## F-statistic: 58.67 on 5 and 149 DF,  p-value: < 2.2e-16

Which shows that the model explains about 65% of variability in target
variable and that ranger learner (Wright & Ziegler,
[2016](#ref-Wright2016)) is somewhat better learner than others.

To predict values at all grids we use:

    meuse.y <- predict(m)

    ## Predicting values using 'getStackedBaseLearnerPredictions'...TRUE

    ## Deriving model errors using ranger package 'quantreg' option...TRUE

Note that, by default, we will predict two outputs:

-   Mean prediction: which is the best unbiased prediction of response,
-   Prediction errors: which is usually predicted as lower and upper 67%
    quantiles based on the quantreg Random Forest algorithm
    (Meinshausen, [2006](#ref-meinshausen2006quantile)), or as a
    standard deviation of bootstraped values,

To determine the prediction errors, we basically fit an independent
random forest model using the five base-learners with setting
`quantreg = TRUE`:

    zinc ~ regr.ranger + regr.xgboost + regr.nnet + regr.ksvm + regr.cvglmnet

The Quantreg technique (Meinshausen,
[2006](#ref-meinshausen2006quantile)) is a non-parameteric method for
determining prediction intervals and users can choose any probability in
the output. It can become, however, computational for large number of
features and trees in the random forest, so have that in mind.

We can plot the predictions and prediction errors next to each other:

    par(mfrow=c(1,2), oma=c(0,0,0,1), mar=c(0,0,4,3))
    plot(raster(meuse.y$pred["response"]), col=R_pal[["rainbow_75"]][4:20],
      main="Predictions spLearner", axes=FALSE, box=FALSE)
      points(meuse, pch="+")
    plot(raster(meuse.y$pred["model.error"]), col=rev(bpy.colors()),
      main="Prediction errors", axes=FALSE, box=FALSE)
      points(meuse, pch="+")

<div class="figure">

<img src="README_files/figure-gfm/map-zinc-1.png" alt="Predicted zinc content based on meuse data set." width="70%" />
<p class="caption">
Predicted zinc content based on meuse data set.
</p>

</div>

This shows that the prediction errors (right plot) are the highest:

-   where the model is getting further away from the training points
    (spatial extrapolation),
-   where individual points with high values can not be explained by
    covariates,
-   where measured values of the response variable are in general high,

#### Estimation of [prediction intervals](http://www.sthda.com/english/articles/40-regression-analysis/166-predict-in-r-model-predictions-and-confidence-intervals/)

We can also look at the lower and upper [prediction
interval](http://www.sthda.com/english/articles/40-regression-analysis/166-predict-in-r-model-predictions-and-confidence-intervals/)
for every location using e.g.:

    sp::over(meuse[1,], meuse.y$pred)

    ##   response model.error q.lwr q.upr
    ## 1 1023.136       227.5   735  1190

where `q.lwr` is the lower and `q.upr` is the 68% probability upper
quantile value. This shows that the 68% probability interval for the
location `x=181072, y=333611` is about 964–1161 which means that the
prediction error (±1 s.d.), at that location, is about 100. Compare with
the actual value sampled at that location:

    meuse@data[1,"zinc"]

    ## [1] 1022

The difference between predicted and observed values is much smaller
than estimated prediction error, but this is because this is the
training location hence the observed and predicted values match very
well. If we refit a model using exactly the same settings we might get
somewhat different map and different values. This is to be expected as
the stacking is done by using (random) 5-fold Cross-validation and
results will always be a bit different. The resulting models and maps,
however, should not be significantly different as this would indicate
that the EML is *unstable*.

#### Predictions using log-transformed target variable

If the purpose of spatial prediction to make a more accurate predictions
of low values of the response, then we can train a model with the
transformed variable:

    meuse$log.zinc = log1p(meuse$zinc)
    m2 <- train.spLearner(meuse["log.zinc"], covariates=meuse.grid[,c("dist","ffreq")], parallel=FALSE)

    ## Converting ffreq to indicators...

    ## Converting covariates to principal components...

    ## Deriving oblique coordinates...TRUE

    ## Fitting a variogram using 'linkfit' and trend model...TRUE

    ## Estimating block size ID for spatial Cross Validation...TRUE

    ## Using learners: regr.ranger, regr.xgboost, regr.nnet, regr.ksvm, regr.cvglmnet...TRUE

    ## Fitting a spatial learner using 'mlr::makeRegrTask'...TRUE

    ## [20:31:21] WARNING: amalgamation/../src/objective/regression_obj.cu:170: reg:linear is now deprecated in favor of reg:squarederror.
    ## [20:31:21] WARNING: amalgamation/../src/objective/regression_obj.cu:170: reg:linear is now deprecated in favor of reg:squarederror.
    ## [20:31:21] WARNING: amalgamation/../src/objective/regression_obj.cu:170: reg:linear is now deprecated in favor of reg:squarederror.
    ## [20:31:21] WARNING: amalgamation/../src/objective/regression_obj.cu:170: reg:linear is now deprecated in favor of reg:squarederror.
    ## [20:31:21] WARNING: amalgamation/../src/objective/regression_obj.cu:170: reg:linear is now deprecated in favor of reg:squarederror.
    ## [20:31:21] WARNING: amalgamation/../src/objective/regression_obj.cu:170: reg:linear is now deprecated in favor of reg:squarederror.
    ## # weights:  103
    ## initial  value 5251.474962 
    ## final  value 63.796063 
    ## converged
    ## # weights:  103
    ## initial  value 4152.664357 
    ## final  value 65.859032 
    ## converged
    ## # weights:  103
    ## initial  value 5200.197525 
    ## final  value 59.739060 
    ## converged
    ## # weights:  103
    ## initial  value 3003.494912 
    ## final  value 63.002217 
    ## converged
    ## # weights:  103
    ## initial  value 5516.001665 
    ## final  value 66.352722 
    ## converged
    ## # weights:  103
    ## initial  value 4482.958256 
    ## final  value 79.790191 
    ## converged

    ## Fitting a quantreg model using 'ranger::ranger'...TRUE

The summary model will usually have a somewhat higher R-square, but the
best learners should stay about the same:

    summary(m2@spModel$learner.model$super.model$learner.model)

    ## 
    ## Call:
    ## stats::lm(formula = f, data = d)
    ## 
    ## Residuals:
    ##      Min       1Q   Median       3Q      Max 
    ## -0.94389 -0.19428 -0.06098  0.13973  1.49077 
    ## 
    ## Coefficients:
    ##               Estimate Std. Error t value Pr(>|t|)    
    ## (Intercept)     2.9935     6.9068   0.433 0.665340    
    ## regr.ranger     0.8270     0.2096   3.945 0.000123 ***
    ## regr.xgboost    0.3085     0.3379   0.913 0.362799    
    ## regr.nnet      -0.6354     1.1667  -0.545 0.586849    
    ## regr.ksvm       0.1635     0.2090   0.782 0.435172    
    ## regr.cvglmnet   0.0298     0.1611   0.185 0.853468    
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Residual standard error: 0.3659 on 149 degrees of freedom
    ## Multiple R-squared:   0.75,  Adjusted R-squared:  0.7416 
    ## F-statistic:  89.4 on 5 and 149 DF,  p-value: < 2.2e-16

We can next predict and then back-transform the values:

    meuse.y2 <- predict(m2)

    ## Predicting values using 'getStackedBaseLearnerPredictions'...TRUE

    ## Deriving model errors using ranger package 'quantreg' option...TRUE

    ## back-transform:
    meuse.y2$pred$response.t = expm1(meuse.y2$pred$response)

<div class="figure">

<img src="README_files/figure-gfm/map-zinc2-1.png" alt="Predicted zinc content based on meuse data set after log-transformation." width="70%" />
<p class="caption">
Predicted zinc content based on meuse data set after log-transformation.
</p>

</div>

If we compare the two predictions (without and with transformation) we
can see that the predictions do not differ that much:

    library(ggridges)
    library(viridis)
    library(ggplot2)
    zinc.df = data.frame(zinc=c(sp::over(meuse, meuse.y$pred["response"])[,1], 
                                sp::over(meuse, meuse.y2$pred["response.t"])[,1],
                                meuse$zinc
                                ))
    zinc.df$type = as.vector(sapply(c("predicted", "log.predicted", "observed"), function(i){rep(i, nrow(meuse))}))
    ggplot(zinc.df, aes(x = zinc, y = type, fill = ..x..)) +
          geom_density_ridges_gradient(scale = 0.95, rel_min_height = 0.01, gradient_lwd = 1.) +
          scale_x_continuous(expand = c(0.01, 0)) +
          ## scale_x_continuous(trans='log2') +
          scale_y_discrete(expand = c(0.01, 0.01)) +
          scale_fill_viridis(name = "Zinc", option = "C") +
          labs(title = "Distributions comparison") +
      theme_ridges(font_size = 13, grid = TRUE) + theme(axis.title.y = element_blank())

    ## Picking joint bandwidth of 111

<div class="figure">

<img src="README_files/figure-gfm/hist-zinc2-1.png" alt="Difference in distributions observed and predicted." width="80%" />
<p class="caption">
Difference in distributions observed and predicted.
</p>

</div>

The observed very high values are somewhat smoothed out but the median
value is about the same, hence the two EML models predict the target
variable without a bias. To estimate the prediction intervals using the
log-tranformed variable we can use:

    x = sp::over(meuse[1,], meuse.y2$pred)
    expm1(x$q.lwr); expm1(x$q.upr)

    ## [1] 832

    ## [1] 1454

In this case the prediction interval is somewhat wider than without
transformation. Note that the log-transformation is not needed for a
non-linear learner such ranger and/or Xgboost, but it is often a good
idea if the focus of prediction is to get a better accuracy for lower
values. For example, if the objective of spatial interpolation is to map
soil nutrient deficiencies, then log-transformation is a good idea as it
will have slightly better accuracy for lower values.

Another advantage of using log-transformation for log-normal variables
is that the prediction intervals would most likely be symmetric, so that
derivation of prediction error (±1 s.d.) can be derived by:

    pe = (q.upr - q.lwr)/2

![alt text](tex/R_logo.svg.png "Zinc") Example: Spatial prediction of soil types (factor-variable)
--------------------------------------------------------------------------------------------------

#### EML for classification

Ensemble Machine Learning can also be used to interpolate factor type
variables e.g. soil types. This is an example with the Ebergotzen
dataset available from the package plotKML (Hengl, Roudier, Beaudette,
Pebesma, & others, [2015](#ref-hengl2015plotkml)):

    library(plotKML)
    data(eberg_grid)
    gridded(eberg_grid) <- ~x+y
    proj4string(eberg_grid) <- CRS("+init=epsg:31467")
    data(eberg)
    coordinates(eberg) <- ~X+Y
    proj4string(eberg) <- CRS("+init=epsg:31467")
    summary(eberg$TAXGRSC)

    ##     Auenboden     Braunerde          Gley         HMoor    Kolluvisol          Moor Parabraunerde  Pararendzina 
    ##            71           790            86             1           186             1           704           215 
    ##       Pelosol    Pseudogley        Ranker       Regosol      Rendzina          NA's 
    ##           252           487            20           376            23           458

In this case the target variable is `TAXGRSC` soil types based on the
German soil classification system. This changes the modeling problem
from regression to classification. We recommend using the following
learners here:

    sl.c <- c("classif.ranger", "classif.xgboost", "classif.nnTrain")

The model training and prediction however looks the same as for the
regression:

    X <- eberg_grid[c("PRMGEO6","DEMSRT6","TWISRT6","TIRAST6")]
    mF <- train.spLearner(eberg["TAXGRSC"], covariates=X, parallel=FALSE)

    ## Converting PRMGEO6 to indicators...

    ## Converting covariates to principal components...

    ## Deriving oblique coordinates...TRUE

    ## Subsetting observations to 79% complete cases...TRUE

    ## Skipping variogram modeling...TRUE

    ## Estimating block size ID for spatial Cross Validation...TRUE

    ## Using learners: classif.ranger, classif.xgboost, classif.nnTrain...TRUE

    ## Fitting a spatial learner using 'mlr::makeClassifTask'...TRUE

To generate predictions use:

    if(!exists("TAXGRSC")){
      TAXGRSC <- predict(mF)
    }

#### Classification accuracy

By default landmap package will predict both hard classes and
probabilities per class. We can check the average accuracy of
classification by using:

    newdata = mF@vgmModel$observations@data
    sel.e = complete.cases(newdata[,mF@spModel$features])
    newdata = newdata[sel.e, mF@spModel$features]
    pred = predict(mF@spModel, newdata=newdata)
    pred$data$truth = mF@vgmModel$observations@data[sel.e, "TAXGRSC"]
    print(calculateConfusionMatrix(pred))

    ##                predicted
    ## true            Auenboden Braunerde Gley Kolluvisol Parabraunerde Pararendzina Pelosol Pseudogley Ranker Regosol
    ##   Auenboden            36         4    0          0             5            0       0          3      0       0
    ##   Braunerde             0       616    0          2            19            8       7         11      0       5
    ##   Gley                  6         8   37          1            11            0       0          5      0       0
    ##   Kolluvisol            0         6    1        103            19            1       1          4      0       3
    ##   Parabraunerde         0        24    0          3           472            0       1         11      0       2
    ##   Pararendzina          0        16    0          0             3          151       5          1      0       0
    ##   Pelosol               0        12    0          1             1            4     151          7      0       1
    ##   Pseudogley            0        35    3          4            22            3       2        327      0      15
    ##   Ranker                0         9    0          0             6            1       1          0      0       0
    ##   Regosol               0        60    0          0            10            1       1         14      0     227
    ##   Rendzina              0         2    0          0             0            0       0          0      0       0
    ##   -err.-                6       176    4         11            96           18      18         56      0      26
    ##                predicted
    ## true            Rendzina -err.-
    ##   Auenboden            0     12
    ##   Braunerde            1     53
    ##   Gley                 0     31
    ##   Kolluvisol           0     35
    ##   Parabraunerde        0     41
    ##   Pararendzina         0     25
    ##   Pelosol              0     26
    ##   Pseudogley           0     84
    ##   Ranker               0     17
    ##   Regosol              0     86
    ##   Rendzina            20      2
    ##   -err.-               1    412

which shows that about 25% of classes are miss-classified and the
classification confusion is especially high for the `Braunerde` class.
Note the result above is based only on the internal training. Normally
one should repeat the process several times using 5-fold or similar (fit
EML, predict errors using resampled values only, repeat).

Predicted probabilities, however, are more interesting because they also
display where EML possibly has problems:

<div class="figure">

<img src="README_files/figure-gfm/map-tax-1.png" alt="Predicted soil types based on EML." width="100%" />
<p class="caption">
Predicted soil types based on EML.
</p>

</div>

The maps show that also in this case geographical distances play a role,
but overall, the features (DTM derivatives and parnt material) seem to
be most important.

In addition to map of probabilities per class, we have also derived
errors per probability, which is in this case can be computed as the
standard deviation between probabilities produced by individual learners
(for classification problems techniques such as quantreg random forest
do not exist):

<div class="figure">

<img src="README_files/figure-gfm/map-tax-error-1.png" alt="Predicted errors per soil types based on s.d. between individual learners." width="100%" />
<p class="caption">
Predicted errors per soil types based on s.d. between individual
learners.
</p>

</div>

In probability space, it is often recommended to use the measures such
as the
[log-loss](https://www.rdocumentation.org/packages/MLmetrics/versions/1.1.1/topics/LogLoss)
which is the difference between the observed and predicted probability.
As a rule of thumb, log-loss values above 0.35 indicate poor accuracy of
predictions, but the threshold number also depends on the number of
classes. In the plot above we can note that, in general, the average
error in maps is relatively low e.g. about 0.07:

    summary(TAXGRSC$pred$error.Parabraunerde)

    ##     Min.  1st Qu.   Median     Mean  3rd Qu.     Max. 
    ## 0.005339 0.065914 0.078798 0.097867 0.098594 0.373669

but there are still many pixels where confusion between classes and
prediction errors are high.

![alt text](tex/R_logo.svg.png "Multi-scale") Example: Spatial prediction of soil organic carbon using 2-scale model
--------------------------------------------------------------------------------------------------------------------

In this example we use EML to make spatial predictions using data-set
with 2 sets of covariates basically at different resolutions 250-m and
100-m. For this we use the Edgeroi data-set (Malone, McBratney, Minasny,
& Laslett, [2009](#ref-malone2009mapping)) used commonly in the soil
science to demonstrate 3D soil mapping of soil organic carbon (g/kg)
based on samples taken from diagnostic soil horizons (multiple depth
intervals):

    data(edgeroi)
    edgeroi.sp <- edgeroi$sites
    coordinates(edgeroi.sp) <- ~ LONGDA94 + LATGDA94
    proj4string(edgeroi.sp) <- CRS("+proj=longlat +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +no_defs")
    edgeroi.sp <- spTransform(edgeroi.sp, CRS("+init=epsg:28355"))
    out.file = paste(getwd(), "output/edgeroi/edgeroi_training_points.gpkg", sep="/")
    #if(!file.exists("out.file")){
    #  writeOGR(edgeroi.sp, out.file, layer="edgeroi_training_points", driver = "GPKG")
    #}

We can fit two independent EML’s using the two sets of covariates and
then produce final predictions by combining them. We will refer to the
two models as coarse and fine-scale models. The fine-scale models will
often be much larger datasets and require serious computing capacity.

#### Coarse-scale model

First we use the 250-m resolution covariates:

    load("input/edgeroi.grids.rda")
    gridded(edgeroi.grids) <- ~x+y
    proj4string(edgeroi.grids) <- CRS("+init=epsg:28355")
    ov2 <- over(edgeroi.sp, edgeroi.grids)
    ov2$SOURCEID <- edgeroi.sp$SOURCEID
    ov2$x = edgeroi.sp@coords[,1]
    ov2$y = edgeroi.sp@coords[,2]

This is a 3D soil data set, so we also use the horizon `DEPTH` to
explain distribution of SOC in soil:

    source("PSM_functions.R")
    h2 <- hor2xyd(edgeroi$horizons)
    ## regression matrix:
    rm2 <- plyr::join_all(dfs = list(edgeroi$sites, h2, ov2))

    ## Joining by: SOURCEID
    ## Joining by: SOURCEID

    formulaStringP2 <- ORCDRC ~ DEMSRT5+TWISRT5+EV1MOD5+EV2MOD5+EV3MOD5+DEPTH
    rmP2 <- rm2[complete.cases(rm2[,all.vars(formulaStringP2)]),]
    str(rmP2[,all.vars(formulaStringP2)])

    ## 'data.frame':    4972 obs. of  7 variables:
    ##  $ ORCDRC : num  8.5 7.3 5 4.7 4.7 ...
    ##  $ DEMSRT5: num  198 198 198 198 198 198 185 185 185 185 ...
    ##  $ TWISRT5: num  19.5 19.5 19.5 19.5 19.5 19.5 19.2 19.2 19.2 19.2 ...
    ##  $ EV1MOD5: num  1.14 1.14 1.14 1.14 1.14 1.14 -4.7 -4.7 -4.7 -4.7 ...
    ##  $ EV2MOD5: num  1.62 1.62 1.62 1.62 1.62 1.62 3.46 3.46 3.46 3.46 ...
    ##  $ EV3MOD5: num  -5.74 -5.74 -5.74 -5.74 -5.74 -5.74 0.01 0.01 0.01 0.01 ...
    ##  $ DEPTH  : num  11.5 17.5 26 55 80 ...

We can now fit an EML directly by using the regression matrix:

    if(!exists("m.oc")){
      m.oc = train.spLearner.matrix(rmP2, formulaStringP2, edgeroi.grids, 
                            parallel=FALSE, cov.model="nugget", cell.size=1000)
    }

The geoR here reports problems as the data set is 3D and hence there are
spatial duplicates. We can ignore this problem and use the pre-defined
cell size of 1-km, although in theory one can also fit 3D variograms.
The results show that the EML model is significant:

    summary(m.oc@spModel$learner.model$super.model$learner.model)

    ## 
    ## Call:
    ## stats::lm(formula = f, data = d)
    ## 
    ## Residuals:
    ##     Min      1Q  Median      3Q     Max 
    ## -22.082  -1.035  -0.110   0.696  62.913 
    ## 
    ## Coefficients:
    ##               Estimate Std. Error t value Pr(>|t|)    
    ## (Intercept)   -0.29891    0.19619  -1.524   0.1277    
    ## regr.ranger    0.98144    0.02501  39.237  < 2e-16 ***
    ## regr.xgboost  -0.04209    0.07314  -0.576   0.5650    
    ## regr.nnet      0.05079    0.02841   1.788   0.0739 .  
    ## regr.ksvm      0.15289    0.02948   5.186 2.24e-07 ***
    ## regr.cvglmnet -0.09829    0.04113  -2.390   0.0169 *  
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Residual standard error: 3.152 on 4966 degrees of freedom
    ## Multiple R-squared:  0.6752, Adjusted R-squared:  0.6749 
    ## F-statistic:  2065 on 5 and 4966 DF,  p-value: < 2.2e-16

We can now predict values at e.g. 5-cm depth by adding a dummy spatial
layer with all fixed values:

    out.tif = "output/edgeroi/pred_oc_250m.tif"
    edgeroi.grids$DEPTH <- 5
    if(!exists("edgeroi.oc")){
      edgeroi.oc = predict(m.oc, edgeroi.grids[,m.oc@spModel$features])
    }
    if(!file.exists(out.tif)){
      writeGDAL(edgeroi.oc$pred["response"], out.tif, 
                options = c("COMPRESS=DEFLATE"))
      writeGDAL(edgeroi.oc$pred["response"], "output/edgeroi/pred_oc_250m_pe.tif", 
                options = c("COMPRESS=DEFLATE"))
    }

which shows the following:

<div class="figure">

<img src="README_files/figure-gfm/map-oc250m-1.png" alt="Predicted SOC content using 250-m covariates." width="100%" />
<p class="caption">
Predicted SOC content using 250-m covariates.
</p>

</div>

The average prediction error in the map is somewhat higher than the
average error from the model fitting:

    summary(edgeroi.oc$pred$model.error)

    ##    Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
    ##  0.5644  3.0055  5.0025  5.6059  7.8250 34.1500

This is because we are predicting the top-soil SOC, which is
exponentially higher at the soil surface and hence average model errors
for top soil should be slightly larger than the mean error for the whole
soil.

#### Fine-scale model

We can now fit the fine-scale model independently from the coarse-scale
model using the 100-m resolution covariates. In this case the 100-m
covariates are based on Landsat 8 and gamma radiometrics images (see
`?edgeroi` for more details):

    load("input/edgeroi.grids100.rda")
    gridded(edgeroi.grids100) <- ~x+y
    proj4string(edgeroi.grids100) <- CRS("+init=epsg:28355")
    ovF <- over(edgeroi.sp, edgeroi.grids100)
    ovF$SOURCEID <- edgeroi.sp$SOURCEID
    ovF$x = edgeroi.sp@coords[,1]
    ovF$y = edgeroi.sp@coords[,2]
    rmF <- plyr::join_all(dfs = list(edgeroi$sites, h2, ovF))

    ## Joining by: SOURCEID
    ## Joining by: SOURCEID

    formulaStringPF <- ORCDRC ~ MVBSRT6+TI1LAN6+TI2LAN6+PCKGAD6+RUTGAD6+PCTGAD6+DEPTH
    rmPF <- rmF[complete.cases(rmF[,all.vars(formulaStringPF)]),]
    str(rmPF[,all.vars(formulaStringPF)])

    ## 'data.frame':    5001 obs. of  8 variables:
    ##  $ ORCDRC : num  8.5 7.3 5 4.7 4.7 ...
    ##  $ MVBSRT6: num  5.97 5.97 5.97 5.97 5.97 5.97 6.7 6.7 6.7 6.7 ...
    ##  $ TI1LAN6: num  31.8 31.8 31.8 31.8 31.8 31.8 14.3 14.3 14.3 14.3 ...
    ##  $ TI2LAN6: num  32.9 32.9 32.9 32.9 32.9 32.9 22.1 22.1 22.1 22.1 ...
    ##  $ PCKGAD6: num  1.39 1.39 1.39 1.39 1.39 1.39 1.06 1.06 1.06 1.06 ...
    ##  $ RUTGAD6: num  0.14 0.14 0.14 0.14 0.14 0.14 0.16 0.16 0.16 0.16 ...
    ##  $ PCTGAD6: num  7.82 7.82 7.82 7.82 7.82 7.82 6.48 6.48 6.48 6.48 ...
    ##  $ DEPTH  : num  11.5 17.5 26 55 80 ...

We fit the 2nd fine-scale model:

    if(!exists("m.ocF")){
      m.ocF = train.spLearner.matrix(rmPF, formulaStringPF, edgeroi.grids100, 
                            parallel=FALSE, cov.model="nugget", cell.size=1000)
    }
    summary(m.ocF@spModel$learner.model$super.model$learner.model)

    ## 
    ## Call:
    ## stats::lm(formula = f, data = d)
    ## 
    ## Residuals:
    ##     Min      1Q  Median      3Q     Max 
    ## -20.414  -0.953  -0.033   0.710  61.120 
    ## 
    ## Coefficients:
    ##                Estimate Std. Error t value Pr(>|t|)    
    ## (Intercept)   -0.464404   0.119807  -3.876 0.000107 ***
    ## regr.ranger    1.158488   0.025040  46.265  < 2e-16 ***
    ## regr.xgboost   0.005880   0.073403   0.080 0.936151    
    ## regr.nnet      0.009628   0.022740   0.423 0.672020    
    ## regr.ksvm     -0.068891   0.029658  -2.323 0.020228 *  
    ## regr.cvglmnet -0.025746   0.029850  -0.863 0.388446    
    ## ---
    ## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
    ## 
    ## Residual standard error: 2.959 on 4995 degrees of freedom
    ## Multiple R-squared:  0.7163, Adjusted R-squared:  0.716 
    ## F-statistic:  2522 on 5 and 4995 DF,  p-value: < 2.2e-16

which shows that the 100-m resolution covariates help make even more
accurate predictions with R-square about 0.7. We can also make
predictions at 5-cm depth by using (this time take almost 6x more time
to compute):

    edgeroi.grids100$DEPTH <- 5
    if(!exists("edgeroi.ocF")){
      edgeroi.ocF = predict(m.ocF, edgeroi.grids100[,m.ocF@spModel$features])
    }
    out.tif = "output/edgeroi/pred_oc_100m.tif"
    if(!file.exists(out.tif)){
      writeGDAL(edgeroi.ocF$pred["response"], out.tif, options = c("COMPRESS=DEFLATE"))
      writeGDAL(edgeroi.ocF$pred["model.error"], "output/edgeroi/pred_oc_100m_pe.tif", options = c("COMPRESS=DEFLATE"))
    }

which shows the following:

<div class="figure">

<img src="README_files/figure-gfm/map-oc100m-1.png" alt="Predicted SOC content using 100-m covariates." width="100%" />
<p class="caption">
Predicted SOC content using 100-m covariates.
</p>

</div>

#### Merge multi-scale predictions

If we compare the coarse scale and fine scale predictions we can notice
that there are indeed differences:

<img src="img/ebergotzen_two_scale_model_ensemble.gif" width="650" />

*Figure: Coarse-scale and fine-scale predictions of soil organic carbon
at 5-cm depth for the Edgeroi study area.*

Overall there is a match between general patterns but there are also
differences. This is to expect as the two models are fitted
independently using completely different covariates. We can merge the
two predictions and produce the final ensemble prediction by using the
following principles:

-   User prediction errors per pixel as weights so that more accurate
    predictions get higher weights,
-   Derive propagated error using the pooled variance based on
    individual predictions and errors,

Before we run this operation, we need to downscale the maps to the same
grid, best using Cubic-splines in GDAL:

    edgeroi.grids100@bbox

    ##       min     max
    ## x  741400  789000
    ## y 6646000 6678100

    outD.file = "output/edgeroi/pred_oc_250m_100m.tif"
    if(!file.exists(outD.file)){
      system(paste0('gdalwarp output/edgeroi/pred_oc_250m.tif ', outD.file,  
             ' -r \"cubicspline\" -te 741400 6646000 789000 6678100 -tr 100 100 -overwrite'))
      system(paste0('gdalwarp output/edgeroi/pred_oc_250m_pe.tif output/edgeroi/pred_oc_250m_100m_pe.tif',
             ' -r \"cubicspline\" -te 741400 6646000 789000 6678100 -tr 100 100 -overwrite'))
    }

We can now read the downscaled predictions, and merge them using the
prediction errors as weights (weighted average per pixel):

    edgeroi.ocF$pred$responseC = readGDAL("output/edgeroi/pred_oc_250m_100m.tif")$band1

    ## output/edgeroi/pred_oc_250m_100m.tif has GDAL driver GTiff 
    ## and has 321 rows and 476 columns

    edgeroi.ocF$pred$model.errorC = readGDAL("output/edgeroi/pred_oc_250m_100m_pe.tif")$band1

    ## output/edgeroi/pred_oc_250m_100m_pe.tif has GDAL driver GTiff 
    ## and has 321 rows and 476 columns

    edgeroi.ocF$pred$responseF = rowSums(
              edgeroi.ocF$pred@data[,c("response", "responseC")] *
              1/(edgeroi.ocF$pred@data[,c("model.error", "model.errorC")]^2) ) / 
              rowSums( 1/(edgeroi.ocF$pred@data[,c("model.error", "model.errorC")]^2) )
    out.tif = "output/edgeroi/pred_oc_100m_merged.tif"
    if(!file.exists(out.tif)){
      writeGDAL(edgeroi.ocF$pred["responseF"], out.tif, options = c("COMPRESS=DEFLATE"))
    }

The final map of predictions is a combination of the two independently
produced predictions:

    plot(raster(edgeroi.ocF$pred["responseF"]), col=R_pal[["rainbow_75"]][4:20],
      main="Merged predictions spLearner", axes=FALSE, box=FALSE)
      points(edgeroi.sp, pch="+")

<div class="figure">

<img src="README_files/figure-gfm/map-2scale-1.png" alt="Merged predictions (coarse+fine scale) of SOC content at 100-m." width="80%" />
<p class="caption">
Merged predictions (coarse+fine scale) of SOC content at 100-m.
</p>

</div>

To merge the prediction errors, we use the pooled variance formula
(Rudmin, [2010](#ref-rudmin2010calculating)):

    edgeroi.ocF$pred$model.errorF = sqrt( rowSums(
              (edgeroi.ocF$pred@data[,c("response", "responseC")] + edgeroi.ocF$pred@data[,c("model.error", "model.errorC")]^2) *
              1/(edgeroi.ocF$pred@data[,c("model.error", "model.errorC")]^2) ) / 
              rowSums( 1/(edgeroi.ocF$pred@data[,c("model.error", "model.errorC")]^2) ) - 
              rowSums(
              edgeroi.ocF$pred@data[,c("response", "responseC")]  *
              1/(edgeroi.ocF$pred@data[,c("model.error", "model.errorC")]^2) ) / 
              rowSums( 1/(edgeroi.ocF$pred@data[,c("model.error", "model.errorC")]^2) ) )
    out.tif = "output/edgeroi/pred_oc_100m_merged_pe.tif"
    if(!file.exists(out.tif)){
      writeGDAL(edgeroi.ocF$pred["model.errorF"], out.tif, options = c("COMPRESS=DEFLATE"))
    }

So in summary, merging multi-scale predictions is a straight forward
process, but it assumes that the reliable prediction errors are
available at both sides. The pooled variance might show higher errors
where predictions between independent models differ significantly. The
2-scale Ensemble Machine Learning method of Predictive Soil Mapping was
used for example to produce predictions of [soil properties and
nutrients of Africa](https://www.isda-africa.com/isdasoil/).

Summary notes
-------------

The tutorial above demonstrates how to use Ensemble Machine Learning for
predictive mapping from numeric to factor to 3D variables. The examples
shown are based on relatively small datasets, but can still become
computational if you add even more learners. You can also follow an
introduction to Ensemble Machine Learning from the [OpenGeoHub Summer
School 2020
recordings](https://www.youtube.com/playlist?list=PLXUoTpMa_9s0Ea--KTV1OEvgxg-AMEOGv).

Please note that the mlr package is discontinued, so some of the example
above might become unstable with time. We are working on migrating the
code in the landmap package to work with the new [mlr3
package](https://mlr3.mlr-org.com/).

If you have a dataset that you have used to test Ensemble Machine
Learning, please come back to us and share your experiences by posting
[an issue](https://github.com/Envirometrix/landmap/issues) / providing a
screenshot of results.

References
----------

<div id="refs" class="references hanging-indent">

<div id="ref-bischl2016mlr">

Bischl, B., Lang, M., Kotthoff, L., Schiffner, J., Richter, J.,
Studerus, E., … Jones, Z. M. (2016). mlr: Machine Learning in R. *The
Journal of Machine Learning Research*, *17*(1), 5938–5942.

</div>

<div id="ref-diggle1998model">

Diggle, P. J., Tawn, J. A., & Moyeed, R. A. (1998). Model-based
geostatistics. *Journal of the Royal Statistical Society: Series C
(Applied Statistics)*, *47*(3), 299–350.

</div>

<div id="ref-hengl2019predictive">

Hengl, T., & MacMillan, R. A. (2019). *Predictive soil mapping with R*
(p. 370). Wageningen: OpenGeoHub Foundation. Retrieved from
<https://soilmapper.org>

</div>

<div id="ref-hengl2018random">

Hengl, T., Nussbaum, M., Wright, M. N., Heuvelink, G. B., & Gräler, B.
(2018). Random forest as a generic framework for predictive modeling of
spatial and spatio-temporal variables. *PeerJ*, *6*, e5518.

</div>

<div id="ref-hengl2015plotkml">

Hengl, T., Roudier, P., Beaudette, D., Pebesma, E., & others. (2015).
PlotKML: Scientific visualization of spatio-temporal data. *Journal of
Statistical Software*, *63*(5), 1–25.

</div>

<div id="ref-lovelace2019geocomputation">

Lovelace, R., Nowosad, J., & Muenchow, J. (2019). *Geocomputation with
r*. CRC Press.

</div>

<div id="ref-malone2009mapping">

Malone, B. P., McBratney, A., Minasny, B., & Laslett, G. (2009). Mapping
continuous depth functions of soil carbon storage and available water
capacity. *Geoderma*, *154*(1-2), 138–152.

</div>

<div id="ref-meinshausen2006quantile">

Meinshausen, N. (2006). Quantile regression forests. *Journal of Machine
Learning Research*, *7*(Jun), 983–999.

</div>

<div id="ref-moller2020oblique">

Møller, A. B., Beucher, A. M., Pouladi, N., & Greve, M. H. (2020).
Oblique geographic coordinates as covariates for digital soil mapping.
*SOIL*, *6*(2), 269–289.

</div>

<div id="ref-Polley2010">

Polley, E. C., & van der Laan, M. J. (2010). *Super learner in
prediction*. U.C. Berkeley Division of Biostatistics. Retrieved from
<https://biostats.bepress.com/ucbbiostat/paper266>

</div>

<div id="ref-rudmin2010calculating">

Rudmin, J. W. (2010). Calculating the exact pooled variance. *arXiv
Preprint*, *1007.1012*.

</div>

<div id="ref-seni2010ensemble">

Seni, G., & Elder, J. F. (2010). *Ensemble methods in data mining:
Improving accuracy through combining predictions*. Morgan & Claypool
Publishers.

</div>

<div id="ref-Wright2016">

Wright, M. N., & Ziegler, A. (2016). ranger: A Fast Implementation of
Random Forests for High Dimensional Data in C++ and R. *Journal of
Statistical Software*, (in press), 18.

</div>

<div id="ref-zhang2012ensemble">

Zhang, C., & Ma, Y. (2012). *Ensemble machine learning: Methods and
applications*. Springer New York. Retrieved from
<https://books.google.nl/books?id=CjAs4stLXhAC>

</div>

</div>
